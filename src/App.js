import './App.css';
import NavBar from './component/NavBar';
import Body from './component/Body';
import ShowCarts from './component/ShowCarts';
import SubNavBar from './component/SubNavBar';
import { Route, withRouter } from 'react-router-dom';
import Electronics from './component/Electronics';
import Jewelery from './component/Jewelery';
import MenClothing from './component/MenClothing';
import WomenClothing from './component/WomenClothing';
const Menu = withRouter(SubNavBar);


function App() {
  return (
    <>
      <Route exact path="/">
        <NavBar />
        <Menu />
        <Body />
      </Route>



      <Route exact path="/carts">
        <NavBar />
        <ShowCarts />
      </Route>

      <Route exact path="/electronics">

        <NavBar />
        <Menu/>
        <Electronics />

      </Route>
      <Route exact path="/jewelery">

        <NavBar />
        <Menu/>
        <Jewelery />

      </Route>
      <Route exact path="/menclothing">

        <NavBar />
        <Menu/>
        <MenClothing />

      </Route>
      <Route exact path="/womenclothing">

        <NavBar />
        <Menu/>
        <WomenClothing />

      </Route>


    </>

  );
}

export default App;
