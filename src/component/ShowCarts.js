import React, { Component } from 'react'

export class ShowCarts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0,

        }
    }
    componentDidMount = () => {

        if (JSON.parse(localStorage.getItem("List")).length > 0) {
            const x = JSON.parse(localStorage.getItem("List"))
            this.setState(prevState => ({
                count: prevState.count + x.length
            }))
        }
    }
    remove = (item) => {
        let data = JSON.parse(localStorage.getItem("List"))
        this.setState({
            count: data.length - 1
        })

        data = data.filter((eachItem) => eachItem.id !== item.id)
        localStorage.setItem('List', JSON.stringify(data))

    }


    render() {
        //console.log(this.state.count)
        return (
            <>
                <div className="container">
                    <div className="row  mt-3">
                        <h1>This is your Cart</h1>
                    </div>


                    {
                        this.state.count !== 0 ?

                            JSON.parse(localStorage.getItem('List')).map((eachItem) => (
                                <div className="row mt-3 border d-flex justify-content-center align-items-center bg-light" key={eachItem.id}>
                                    <div className="col">
                                        <img src={eachItem.image} width="100" height="100" className="d-inline-block align-top" alt="" />
                                    </div>
                                    <div className="col">
                                        <h5>{eachItem.name}</h5>
                                    </div>
                                    <div className="col">
                                        <h5>Qty : {eachItem.qty}  </h5>
                                    </div>
                                    <div className="col">
                                        <h5>Price : Rs {eachItem.price}</h5>
                                    </div>
                                    <div className="col">
                                        <h5>Total Cost : Rs {eachItem.total}</h5>
                                    </div>
                                    <div className="col">
                                        <button type="button" className="btn btn-outline-danger" onClick={(e) => this.remove(eachItem)}>Remove from cart</button>
                                    </div>

                                </div>

                            )) :
                            <div className="alert alert-success" role="alert">
                                No item in Carts
                            </div>
                    }

                </div>
            </>
        )
    }
}

export default ShowCarts
