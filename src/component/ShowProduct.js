import React, { Component } from 'react'
// import veg from '../image/veg.png'
// import nonveg from '../image/nonveg.png'
// import Axios from 'axios'

export class ShowProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: 1,
        }
    }

    addInCart = (e, product) => {

        let a = JSON.parse(localStorage.getItem('List')) || [];

        const exist = a.find((data) => data.id === product.id)
        if (!exist) {

            product.qty = this.state.value
            product.total = product.price * product.qty
            a.push(product)
            localStorage.setItem('List', JSON.stringify(a));



        } else {
            a = a.map((data) => {
                if (data.id === product.id) {
                    data.qty = data.qty + this.state.value
                    data.total = data.price * data.qty
                }
                return data
            })
            localStorage.setItem('List', JSON.stringify(a));

        }
    }

    addCart = () => {

        this.setState(prevState => ({
            value: prevState.value + 1
        }))
    }
    removeCart = () => {
        if (this.state.value === 1) {
            return
        } else {
            this.setState(prevState => ({
                value: prevState.value - 1
            }))

        }

    }
    //col-xs-4 col-sm-4 col-md-3 col-lg-2

    render() {
        return (
            <>
                <div className="col-6 col-sm-4 col-lg-3 col-xl-2 p-4 border " key={this.props.data.id}>

                    <div className="row d-flex justify-content-center">
                        <img src={this.props.data.image} width="140" height="140" className="" alt="..." />
                    </div>
                    <div className="row mx-0"   >
                        <div className="col-12 px-0" style={mystyle} >
                            <h6 className="pt-1">{this.props.data.title}</h6>


                        </div>

                    </div>
                    <h6 className="pt-1">Price Rs {this.props.data.price}</h6>

                    <span className="pt-1">Category {this.props.data.category}</span>
                    <div className="row pt-2">
                        <span className=" col input-group-text">Qty</span>
                        <button type="button" className="col btn btn-outline-danger" onClick={(e) => this.removeCart()}>-</button>
                        <span className=" col input-group-text"> {this.state.value}</span>
                        <button type="button" className="col btn btn-outline-primary" onClick={(e) => this.addCart()}>+</button>
                        <button type="button" className="col btn btn-outline-primary" onClick={(e) => this.addInCart(e, this.props.data)}>Add</button>
                    </div>


                </div>
            </>
        )
    }
}
const mystyle = {
    height: "90px"
}

export default ShowProduct