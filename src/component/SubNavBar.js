import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Select from 'react-select'

// import makeAnimated from 'react-select/animated';
import { BrowserRouter as Router } from 'react-router-dom';


class SubNavBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            category: [

                { value: "", label: "All" },
                { value: "electronics", label: "Electronics" },
                { value: "jewelery", label: "Jewelery" },
                { value: "menclothing", label: "Men Clothing" },
                { value: "womenclothing", label: "Women Clothing" }
            ],
            value: "All"
        }
    }
    handleChange = (value) => {
        this.setState({
            value: value.value
        })
        this.props.history.push(`/${value.value}`);


    }


    render() {
        const data = this.state.category
        const selectedValue = this.state.value
        return (
            <div className="container">
                <div className="row my-1">
                    <div className="col-6  col-sm-3 col-md-3 col-lg-3">
                        <Router>
                            <Select
                                onChange={this.handleChange}
                                className="mt-4 col-md-8 col-offset-4"
                                isSearchable={false}
                                // isMulti
                               // defaultValue={this.state.value}
                               placeholder = "Select Category"
                               value={data.find(obj => obj.value === selectedValue)} 
                                 options={this.state.category}
                            />
                        </Router>



                    </div>
                    <div className="col-6 col-sm-3 col-md-3 col-lg-3" style = {mystyle}>
                        <Link to="/carts"><button type="button" className="btn btn-outline-primary px-3 py-2">Show Carts </button> </Link>
                    </div>
                </div>

            </div>
        )
    }
}

const mystyle = {
    marginTop : "21px"
}

export default SubNavBar

