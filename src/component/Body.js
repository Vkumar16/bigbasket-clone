import React, { Component } from 'react'
import ShowProduct from './ShowProduct'
import Axios from 'axios'
import loader from '../loader.jpg'
import Search from '../image/search1.png'

class Body extends Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            loader: false,
            error: false,
            searchValue: "",
            searchProducts: []

        }
    }
    onChange = (e) => {
        const { products } = this.state
        const value = e.target.value
        this.setState({
            searchValue: value
        })

        if (products.length > 0 && value.length > 0) {
            const data = products.filter((product) => 
                
                (product.title.toLowerCase().includes(value.toLowerCase()) || product.category.toLowerCase().includes(value.toLowerCase()) || product.description.toLowerCase().includes(value.toLowerCase())) 
            )
            this.setState({
                searchProducts: data
            })

        } else {
            this.setState({
                searchProducts: []
            })
        }


    }
    componentDidMount = async () => {

        const products = await Axios.get('https://fakestoreapi.com/products')
            .then(({ data }) => data)
            .catch((error) => {
                this.setState({
                    error: true
                })
            })

        this.setState({
            products,
            loader: true
        })

    }
    render() {

        return (

            <>
                <form className="container d-flex justify-content-center">
                    <input type="search" placeholder="Search for product ...." onChange={this.onChange} />
                    <img src={Search} width="30" height="30" className="d-inline-block align-top " alt="" />

                </form>

                {
                    this.state.error
                        ?
                        <div className="container my-2">
                            <div className="alert alert-danger" role="alert">
                                <h1 className="d-flex justify-content-center m-20">Page is not loading...... please try again</h1>
                            </div>
                        </div>
                        : this.state.searchValue.length > 0 && this.state.searchProducts.length > 0
                            ?

                            <div className="container mt-3">
                                <div className="row justify-content-center">
                                    {
                                        this.state.searchProducts.map((product) => (

                                            <ShowProduct key={product.id} data={product} ></ShowProduct>
                                        ))


                                    }


                                </div>
                            </div>
                            : this.state.searchValue.length > 0 && this.state.searchProducts.length === 0
                                ?
                                <div className="container my-2">
                                    <div className="alert alert-danger" role="alert">
                                        <h1 className="d-flex justify-content-center m-20">No result found...</h1>
                                    </div>
                                </div>
                                :

                                this.state.loader ?
                                    <div className="container mt-3">
                                        <div className="row ">
                                            {
                                                this.state.products.map((product) => (

                                                    <ShowProduct key={product.id} data={product} ></ShowProduct>
                                                ))


                                            }


                                        </div>
                                    </div>

                                    :
                                    <div className="container">
                                        <div className="row" >
                                            <h3 className="d-flex justify-content-center m-20">Loading Page .......</h3>
                                        </div>
                                        <div className="row" >
                                            <div className="d-flex justify-content-center m-20"> <img src={loader} width="60" height="60" alt="" /></div>
                                        </div>
                                    </div>

                }
            </>

        )
    }
}



export default Body
