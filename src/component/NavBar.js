import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import bigbasket from '../image/bigbasket.png'

class NavBar extends Component {

    render() {
        return (
            <>
                <div className="container">
                    <div className="row align-items-center">

                        <div className="col-sm-3 col-md-3 col-lg-4">
                            <Link to="/">
                                <img src={bigbasket} width="200" height="150" className="d-inline-block align-top " alt="" />
                            </Link>
                        </div>

                        <div className="col-sm-9 col-md-9 col-lg-8">
                            <div className=" d-flex">
                                <p>8936803398 |</p>
                                <p>Bengaluru |</p>

                                <p>Login |</p>
                                <p>Sign up |</p>


                            </div>
                        </div>
                    </div>

                </div>
            </>

        )

    }
}
// const mystyle = {
//     backgroundColor: "#FFA900"
//     //  "#FF7600",
// }
// const mystyle1 = {
//     backgroundColor: "#FFA900",
// }


export default NavBar
